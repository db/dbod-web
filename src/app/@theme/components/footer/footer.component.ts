import { Component } from '@angular/core';

@Component({
    selector: 'cern-ngx-footer',
    styleUrls: [ './footer.component.scss' ],
    template: `
    <span class="created-by">Created with ♥ by <b><a href="https://information-technology.web.cern.ch/services/database-on-demand" target="_blank">DBOD</a></b> 2021 </span>
    <div class="socials">
      <a href="https://cern.ch/dbod-user-guide" target="_blank"><span>User Guide</span></a>
      <a href="https://cern.ch/dbod-admin-guide" target="_blank" ><span>Admin Guide</span></a>
      <a href="https://cern.service-now.com/service-portal?id=sc_cat_item&name=incident&se=database-on-demand" target="_blank"><span>Report an incident</span></a>
      <a href="https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=database-on-demand" target="_blank"><span>Submit a request</span></a>
      </div>
  `,
})
export class FooterComponent {}
