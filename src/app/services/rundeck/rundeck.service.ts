import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { AuthenticationService } from '../../services/authentication/authentication.service';

@Injectable()
export class RundeckService {
  constructor(private http: HttpClient, private authService: AuthenticationService) { }

  post(path: string, params?) {
    return new Promise((resolve, reject) => {
      this.authService.loadUser().then(() => {
        this.http.post('./api/v1/rundeck/' + path, params, { headers: new HttpHeaders({ 'jwt-session': this.authService.user.jwt }) })
          .subscribe((data: any) => {
            if (!data.response.entries) {
              resolve(data.response.id);
            } else {
              resolve(data.response.entries[0]);
            }
          }, (err) => reject(err));
      });
    });
  }
}
