import { Observable } from 'rxjs/Rx';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { AuthenticationService } from '../../services/authentication/authentication.service';

@Injectable()
export class FileDownloaderService {
  constructor(private http: HttpClient, private authService: AuthenticationService) { }

  getLogFile(url): Promise<JSON> {
    return new Promise((resolve, reject) => {
      this.authService.loadUser().then(() => {
        this.http.get('/download/log-file', { 'params': { 'url': url } })
          .subscribe((res: JSON) => resolve(res));
      }, (err) => reject(err));
    });
  }

  getValidation(newFile, instance, version, filetype) {
    return new Promise((resolve, reject) => {
      this.authService.loadUser().then(() => {
        this.http.post('/validate/', { 'newFile': newFile, 'instance': instance, 'version': version, 'filetype': filetype },
          { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) })
          .subscribe((res) => resolve(res));
      }, (err) => reject(err));
    });
  }
}
