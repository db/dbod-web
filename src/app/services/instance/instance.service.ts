import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { AuthenticationService } from '../../services/authentication/authentication.service';

@Injectable()
export class InstanceService {
  constructor(private http: HttpClient, private authService: AuthenticationService) { }

  getInstances() {
    return new Promise((resolve, reject) => {
      this.authService.loadUser().then(() => {
        this.http.get('./api/v1/instance', { headers: new HttpHeaders({ 'jwt-session': this.authService.user.jwt }) })
          .subscribe((res) => resolve(res));
      });
    });
  }


  post(instanceId, attribute, fieldName, fieldValue) {
    return new Promise((resolve, reject) => {
      const url = attribute ? './api/v1/instance/' + instanceId + '/attribute/' + fieldName : './api/v1/instance/' + instanceId;
      let req;
      if (attribute) {
        req = fieldValue;
      } else {
        req = {};
        req[fieldName] = fieldValue;
      }

      const toReturn = {
        message: '',
        status: 0,
      };

      this.http.post(url, req, { headers: new HttpHeaders({ 'jwt-session': this.authService.user.jwt }), observe: 'response' })
        .subscribe((res: HttpResponse<any>) => {
          toReturn.status = res.status;
          if (res.ok) {
            toReturn.message = 'Change applied with success.';
          }
          resolve(toReturn);
        }, (err: any) => {
          toReturn.message = err.message;
          toReturn.status = err.status;
          reject(toReturn);
        });
    });
  }

  put(instanceId, attribute, fieldName, fieldValue) {
    return new Promise((resolve, reject) => {
      const url = attribute ? './api/v1/instance/' + instanceId + '/attribute/' + fieldName : './api/v1/instance/' + instanceId;
      let req;
      if (attribute) {
        req = fieldValue;
      } else {
        req = {};
        req[fieldName] = fieldValue;
      }

      const toReturn = {
        message: '',
        status: 0,
      };

      this.http.put(url, req, { headers: new HttpHeaders({ 'jwt-session': this.authService.user.jwt }), observe: 'response' })
        .subscribe((res: HttpResponse<any>) => {
          toReturn.status = res.status;
          if (res.ok) {
            toReturn.message = 'Change applied with success.';
          }
          resolve(toReturn);
        }, (err: any) => {
          toReturn.message = err.message;
          toReturn.status = err.status;
          reject(toReturn);
        });
    });
  }

  /***
   * Updates all the instance data (fields and attributes)
   * It will update all the instance information provided in @param body
   * @param instanceId
   * @param body
   */

  updateData(instanceId, body) {
    return new Promise((resolve, reject) => {
      const url = './api/v1/instance/' + instanceId;
      const toReturn = {
        message: '',
        status: 0,
      };

      this.http.put(url, body, { headers: new HttpHeaders({ 'jwt-session': this.authService.user.jwt }), observe: 'response' })
        .subscribe((res: HttpResponse<any>) => {
          toReturn.status = res.status;
          if (res.ok) {
            toReturn.message = 'Change applied with success.';
          }
          resolve(toReturn);
        }, (err: any) => {
          toReturn.message = err.message;
          toReturn.status = err.status;
          reject(toReturn);
        });
    });
  }
  delete(instanceId, attribute, fieldName) {
    return new Promise((resolve, reject) => {
      const url = attribute ? './api/v1/instance/' + instanceId + '/attribute/' + fieldName : './api/v1/instance/' + instanceId;

      const toReturn = {
        message: '',
        status: 0,
      };

      this.http.delete(url, { headers: new HttpHeaders({ 'jwt-session': this.authService.user.jwt }) }).subscribe((res: any) => {
        // console.log(res);
        if (!res.ok) {
          toReturn.message = res.message;
          toReturn.status = res.status;
        } else {
          toReturn.message = 'Change applied with success.';
          toReturn.status = 200;
        }
        resolve(toReturn);
      }, (err: any) => {
        toReturn.message = err.message;
        toReturn.status = err.status;
        reject(toReturn);
      });
    });
  }
}
