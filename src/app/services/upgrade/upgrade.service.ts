import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { AuthenticationService } from '../../services/authentication/authentication.service';


@Injectable()
export class UpgradeService {
  constructor(private http: HttpClient, private authService: AuthenticationService) {}



  getUpgrades() {
    return new Promise((resolve, reject) => {
      this.authService.loadUser().then(() => {
        this.http.get('./api/v1/upgrade', { headers: new HttpHeaders({ 'jwt-session': this.authService.user.jwt }) })
          .subscribe((res) => resolve(res));
      });
    });
  }

  post(upgradeData) {
    return new Promise((resolve, reject) => {
      const url =  './api/v1/upgrade/create';
      const toReturn = {
        message: '',
        status: 0,
      };

      this.http.post(url, upgradeData, { headers: new HttpHeaders({ 'jwt-session': this.authService.user.jwt }), observe: 'response' })
        .subscribe((res: HttpResponse<any>) => {
          toReturn.status = res.status;
          if (res.ok) {
            toReturn.message = 'success.';
          }
          resolve(toReturn);
        }, (err: any) => {
          toReturn.message = err.message;
          toReturn.status = err.status;
          reject(toReturn);
        });
    });
  }



  delete(upgradeId) {
    return new Promise((resolve, reject) => {
      const url = './api/v1/upgrade/' + upgradeId;
      const toReturn = {
        message: '',
        status: 0,
      };

      this.http.delete(url, { headers: new HttpHeaders({ 'jwt-session': this.authService.user.jwt }) }).subscribe((res: any)  => {
        // If the response is not empty we check the status code
        if (res !== null) {
          toReturn.message = res.message;
          toReturn.status = res.status;
        } else {
          toReturn.message = 'Success';
          toReturn.status = 204;
        }
        resolve(toReturn);
      }, (err: any) => {
        toReturn.message = err.message;
        toReturn.status = err.status;
        reject(toReturn);
      });
    });
  }


  put(upgradeData, upgradeId) {
    return new Promise((resolve, reject) => {
      const url = './api/v1/upgrade/' + upgradeId;

      const toReturn = {
        message: '',
        status: 0,
      };

      this.http.put(url, upgradeData, { headers: new HttpHeaders({ 'jwt-session': this.authService.user.jwt }), observe: 'response' })
        .subscribe((res: HttpResponse<any>) => {
          toReturn.status = res.status;
          if (res.ok) {
            toReturn.message = 'Change applied with success.';
          }
          resolve(toReturn);
        }, (err: any) => {
          toReturn.message = err.message;
          toReturn.status = err.status;
          reject(toReturn);
        });
    });
  }

}
