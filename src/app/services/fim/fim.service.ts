import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { AuthenticationService } from '../../services/authentication/authentication.service';


@Injectable()
export class FimService {
  constructor(private http: HttpClient, private authService: AuthenticationService) {}

    getFimData(instanceName) {
      return new Promise((resolve, reject) => {
        const toReturn = {
          message: '',
          status: 0,
          data: undefined,
        };
        this.authService.loadUser().then(() => {
          this.http.get('./api/v1/fim/' + instanceName, { headers: new HttpHeaders({ 'jwt-session': this.authService.user.jwt }) })
            .subscribe((res: HttpResponse<any>) => {
              toReturn.status = res.status;
              toReturn.data = res['data'];
              if (res.ok) {
                toReturn.message = 'FIM data loaded successfully';
              }
              resolve(toReturn);
            }, (err: any) => {
              toReturn.message = err.message;
              toReturn.status = err.status;
              reject(toReturn);
            } );
        });
      });
    }


}
