import { Component, DoCheck } from '@angular/core';
import { MENU_ITEMS } from './pages-menu';
import { AuthenticationService } from '../services/authentication/authentication.service';

@Component({
  selector: 'cern-ngx-pages',
  template: `
  <cern-ngx-one-column-layout>
  <nb-menu [items]="menu"></nb-menu>
  <router-outlet></router-outlet>
  </cern-ngx-one-column-layout>
  `,
})
export class PagesComponent implements DoCheck  {

  menu = MENU_ITEMS;

  constructor(private authService: AuthenticationService) { }

  ngDoCheck() {
    if (this.authService.user !== undefined) {
      this.menu[1].hidden = !this.authService.user.isAdmin;
    }
  }
}
