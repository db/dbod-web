import { Component } from '@angular/core';

@Component({
  selector: 'cern-ngx-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {

  jobsTitle: string = 'Jobs Overview';
  instancesTitle: string = 'Instances Overview';

  constructor() { }
}
