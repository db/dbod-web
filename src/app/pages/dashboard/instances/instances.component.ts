import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { InstanceService } from '../../../services/instance';
import { LocalDataSource } from 'ng2-smart-table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AuthenticationService } from '../../../services/authentication/authentication.service';

@Component({
  selector: 'cern-ngx-instances',
  providers: [InstanceService],
  templateUrl: './instances.component.html',
  styleUrls: ['./instances.component.scss'],
})
export class InstancesComponent implements OnInit {
  displayedColumns: string[] = ['state', 'name', 'port', 'owner', 'egroup', 'project'];
  dataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private _instanceService: InstanceService, public authService: AuthenticationService) {
    this.authService
      .loadUser().then(() => {
        if (this.authService.user && this.authService.user.isAdmin) {
          this.displayedColumns = ['state', 'name', 'port', 'owner', 'egroup', 'project', 'host'];
        } else {
          this.displayedColumns = ['state', 'name', 'port', 'owner', 'egroup', 'project'];
        }
      });
    if (this.authService.user && this.authService.user.isAdmin) {
      this.dataSource = new MatTableDataSource(
        [{ 'state': '', 'name': '', 'owner': '', 'egroup': '', 'project': '', 'port': '', 'host': '' }]);
    } else {
      this.dataSource = new MatTableDataSource([{ 'state': '', 'name': '', 'owner': '', 'egroup': '', 'project': '', 'port': '' }]);
    }
  }

  ngOnInit() {
    this._instanceService.getInstances().then((res) => {
      this.dataSource = new MatTableDataSource(res['response'].filter(instance => instance.status === 'ACTIVE'));
      this.dataSource.sortingDataAccessor = (item, property) => {
        switch (property) {
          case 'port': return item.attributes.port;
          default: return item[property];
        }
      };
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
