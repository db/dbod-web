import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NbCardModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { HelpComponent } from './help.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NbCardModule,
    ThemeModule,
  ],
  declarations: [
    HelpComponent,
  ],
})
export class HelpModule { }
