import {Component, Inject, OnInit} from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RundeckService } from '../../../../services/rundeck/rundeck.service';
import { InstanceService} from '../../../../services/instance';
import { FimService} from '../../../../services/fim';

@Component({
  selector: 'cern-ngx-expired-pending-dialog',
  templateUrl: './expired-pending-dialog.component.html',
})
export class ExpiredPendingDialogComponent implements OnInit {
  Action = {
    Validate: 0,
    Rescue: 1,
    Destroy: 2,

  };
  Action_status = {
    Operating: 0,
    Loading: 1,
    Success: 2,
    Error: 3,
    FimError: 4,
  };

  action;
  action_status;
  resMessage: string;
  resStatus: number;
  instanceData: any;
  fimData: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private rundeckService: RundeckService,
              private instanceService: InstanceService, private fimService: FimService) {

    const buttonAction = Number(data.buttonAction);
    this.instanceData = data.instanceData;
    this.action_status = this.Action_status['Operating'];
    if (buttonAction === this.Action['Validate']) {
      this.action = this.Action['Validate'];
    } else {
      if (buttonAction === this.Action['Rescue']) {
        this.action = this.Action['Rescue'];
      } else {
        if (buttonAction === this.Action['Destroy']) {
          this.action = this.Action['Destroy'];
        }
      }
    }
  }

  ngOnInit() {
    this.loadFimData();
  }
  loadFimData() {
    // Used to check if the instance exists in the fim table
    this.fimService.getFimData(this.instanceData.name)
      .then( res => {
      this.fimData = res['data'];
    }, (err) => {
     /*
      If the error returned by the API is different from 404 (not found),
      there might be other issues, so we do not allow the deletion by instantiating 'fimData' variable.
      */
     if (err.status !== 404) {
       this.fimData = ' ';
     }

    });
  }
  // Workaround until the new FIM
  validateInstance() {
    // window.open("https://resources.web.cern.ch/resources/Manage/Requests.aspx", "_blank");
  }

  rescueInstance() {
    this.action_status = this.Action_status['Loading'];
    let new_expiry_date = null;
    if (this.instanceData.category === 'TEST') {
      const now = new Date();
      new_expiry_date = new Date(now.setMonth(now.getMonth( ) + 6)).toJSON();
    }

    this.instanceData.expiry_date = new_expiry_date;
    this.instanceData.status = 'ACTIVE';

    this.action_status = this.Action_status['Success'];

    this.instanceService.updateData(this.instanceData.id, this.instanceData ).then((data: any) => {
      this.resStatus = data.status;
      this.resMessage = data.message;
      this.action_status = this.Action_status['Success'];
    }, (err) => {
      this.resStatus = err.status;
      this.resMessage = err.message;
      this.action_status = this.Action_status['Error'];
    });


  }

  destroyInstance() {
    this.action_status = this.Action_status['Loading'];
    const params = {
      options : {
        instance_name: this.instanceData.name,
      },
    };
    if (this.fimData === undefined) {

      this.rundeckService.post('job/destroy-instance/' + this.instanceData.name + '?async' )
        .then( (data: any) => {
          this.resStatus = data.status;
          this.resMessage = 'Job successfully submitted';
          this.action_status = this.Action_status['Success'];
        }, (err) => {
          this.resStatus = err.status;
          this.resMessage = err.message;
          this.action_status = this.Action_status['Error'];
        });
    } else {
      this.action_status = this.Action_status['FimError'];
    }

  }

}
