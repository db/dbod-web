import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { InstanceService } from '../../../services/instance';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { ExpiredPendingDialogComponent } from './expired-pending-dialog/expired-pending-dialog.component';

@Component({
  selector: 'cern-ngx-expired-pending',
  providers: [InstanceService],
  templateUrl: './expired-pending.component.html',
  styleUrls: ['./expired-pending.component.scss'],
})
export class ExpiredPendingComponent implements OnInit {

  displayedColumns: string[] = ['name', 'owner', 'egroup', 'project', 'type', 'category', 'expiry_date', 'validate', 'rescue', 'destroy'];
  dataSource;
  expiredPendingList;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public authService: AuthenticationService, private _InstanceService: InstanceService, public dialog: MatDialog) {
    this.dataSource = new MatTableDataSource([{ 'name': '', 'owner': '', 'egroup': '', 'project': '', 'type': '', 'category': '', 'expiry_date': '' }]);
  }
  ngOnInit() {
    this.loadData();
  }
  loadData() {
    this._InstanceService.getInstances().then((res) => {
      const currentDate = new Date();
      const expiredPending = [];
      for (let i = 0; i < res['response'].length; i++) {
        const expiryDate = new Date(res['response'][i].expiry_date);
        if ((res['response'][i].state === 'AWAITING_APPROVAL') || (res['response'][i].expiry_date != null && expiryDate < currentDate)) {
          expiredPending.push(res['response'][i]);
        }
      }
      this.expiredPendingList = expiredPending;
      this.dataSource = new MatTableDataSource(expiredPending);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  changeInstance(value, instanceName) {
    if ( value === '0') {
      window.open('https://resources.web.cern.ch/resources/Manage/Requests.aspx', '_blank');
    } else {
    const selectedInstance = this._getInstanceByName(instanceName);
    const dialogRef = this.dialog.open(ExpiredPendingDialogComponent, {
      data: {
        buttonAction: value,
        instanceData: selectedInstance,
      },
    });
    dialogRef.afterClosed().subscribe(() => {
      this.loadData();
    });
    }
  }

  private _getInstanceByName(name) {
    for (let i = 0; i < this.expiredPendingList.length; i++) {
       if (this.expiredPendingList[i]['name'] === name) {
         return this.expiredPendingList[i];
       }
    }
  return '';
  }

}
