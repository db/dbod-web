import {Component, Inject, Input} from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UpgradeService } from '../../../../services/upgrade/upgrade.service';

@Component({
  selector: 'cern-ngx-upgrade-edit-dialog',
  templateUrl: './upgrade-edit-dialog.component.html',
})
export class UpgradeEditDialogComponent {
  State = {
    Edit_upgrade: 0,
    Loading: 1,
    Success: 2,
    Error: 3,
  };
  upgradeData: any;
  state;
  resMessage: string;
  resStatus: number;
  dbTypes = [
    {value: 'MYSQL', viewValue: 'MYSQL'},
    {value: 'PG', viewValue: 'PG'},
    {value: 'InfluxDB', viewValue: 'InfluxDB'},
  ];
  categories = [
    {value: 'PROD', viewValue: 'PROD'},
    {value: 'TEST', viewValue: 'TEST'},
    {value: 'REF', viewValue: 'REF'},
  ];
  selectedDbType: string;
  selectedCategory: string;
  versionFrom = '';
  versionTo = '';

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private _upgradeService: UpgradeService) {
    this.state = this.State['Edit_upgrade'];
    this.upgradeData = data.upgradeData;
    this.selectedDbType = data.upgradeData.db_type;
    this.selectedCategory = data.upgradeData.category;
    this.versionFrom = data.upgradeData.version_from;
    this.versionTo = data.upgradeData.version_to;
  }

  sendRequest() {
    this.state = this.State['Loading'];

    this.upgradeData = {
      'db_type': this.selectedDbType,
      'category': this.selectedCategory,
      'version_from': this.versionFrom,
      'version_to': this.versionTo,
      'id' : this.upgradeData.id,
    };
    this._upgradeService.put(this.upgradeData, this.upgradeData.id)
      .then( (data: any) => {
        this.resStatus = data.status;
        this.resMessage = data.message;
        this.state = this.State['Success'];
      }, (err) => {
        this.resStatus = err.status;
        this.resMessage = err.message;
        this.state = this.State['Error'];
      });
  }



}
