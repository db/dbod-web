import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { InstanceService } from '../../../services/instance';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort} from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import {UpgradeService} from '../../../services/upgrade';
import {MatDialog} from '@angular/material/dialog';
import {UpgradeDestroyDialogComponent} from './upgrade-destroy-dialog/uprade-destroy-dialog.component';
import {UpgradeCreateDialogComponent} from './upgrade-add-dialog/upgrade-create-dialog.component';
import {UpgradeEditDialogComponent} from './upgrade-edit-dialog/upgrade-edit-dialog.component';

@Component({
  selector: 'cern-ngx-upgrades',
  providers: [InstanceService],
  templateUrl: './upgrades.component.html',
  styleUrls: ['./upgrades.component.scss'],
})
export class UpgradesComponent implements OnInit {

  displayedColumns: string[] = ['db_type', 'category', 'version_from', 'version_to', 'destroy', 'edit'];
  dataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public authService: AuthenticationService, public dialog: MatDialog, private _instanceService: InstanceService,
              private _upgradeService: UpgradeService) {
    this.dataSource = new MatTableDataSource([{'db_type': '', 'category': '', 'version_from': '', 'version_to': ''}]);
  }

  ngOnInit() {
    this.loadData();
  }
  loadData() {
    this._upgradeService.getUpgrades().then((res) => {
      this.dataSource = new MatTableDataSource(res['response']);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  destroyUpgrade(upgradeData) {
    const dialogRef = this.dialog.open(UpgradeDestroyDialogComponent, {
      data: {
        upgradeData: upgradeData,
      },
    });
    dialogRef.afterClosed().subscribe(() => {
      this.loadData();
    });
 }
  createUpgrade() {
    const dialogRef = this.dialog.open(UpgradeCreateDialogComponent, {
    });
    dialogRef.afterClosed().subscribe(() => {
      this.loadData();
    });
  }
  editUpgrade(upgradeData) {
    const dialogRef = this.dialog.open(UpgradeEditDialogComponent, {
      data: {
        upgradeData: upgradeData,
      },
    });
    dialogRef.afterClosed().subscribe(() => {
     this.loadData();
    });
  }

  }
