import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UpgradeService } from '../../../../services/upgrade/upgrade.service';

@Component({
  selector: 'cern-ngx-upgrade-destroy-dialog',
  templateUrl: './upgrade-destroy-dialog.component.html',
})
export class UpgradeDestroyDialogComponent {
  State = {
    Confirm_delete: 0,
    Loading: 1,
    Success: 2,
    Error: 3,
  };
  upgradeData: any;
  state;
  resMessage: string;
  resStatus: number;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private _upgradeService: UpgradeService) {
    this.state = this.State['Confirm_delete'];
    this.upgradeData = data.upgradeData;
  }

  sendRequest() {
    this.state = this.State['Loading'];

    this._upgradeService.delete(this.upgradeData.id)
      .then( (data: any) => {
        this.resStatus = data.status;
        this.resMessage = data.message;
        this.state = this.State['Success'];
      }, (err) => {
        this.resStatus = err.status;
        this.resMessage = err.message;
        this.state = this.State['Error'];
      });
  }
}
