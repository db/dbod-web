import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminInstancesComponent } from './admin-instances.component';
import { ExpiredPendingComponent } from './expired-pending/expired-pending.component';
import { UpgradesComponent } from './upgrades/upgrades.component';
import { InstanceService } from '../../services/instance';

import { MatSliderModule } from '@angular/material/slider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatBadgeModule } from '@angular/material/badge';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatIconModule } from '@angular/material/icon';
import { ExpiredPendingDialogComponent } from './expired-pending/expired-pending-dialog/expired-pending-dialog.component';
import {UpgradeService} from '../../services/upgrade';
import {UpgradeDestroyDialogComponent} from './upgrades/upgrade-destroy-dialog/uprade-destroy-dialog.component';
import {UpgradeCreateDialogComponent} from './upgrades/upgrade-add-dialog/upgrade-create-dialog.component';
import {FormsModule} from '@angular/forms';
import {UpgradeEditDialogComponent} from './upgrades/upgrade-edit-dialog/upgrade-edit-dialog.component';
import {FimService} from '../../services/fim';

@NgModule({
  imports: [
    CommonModule,
    MatSliderModule,
    MatExpansionModule,
    MatSelectModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatDialogModule,
    MatBadgeModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatTooltipModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    FormsModule,
  ],
  declarations: [
    AdminInstancesComponent,
    ExpiredPendingComponent,
    UpgradesComponent,
    ExpiredPendingDialogComponent,
    UpgradeDestroyDialogComponent,
    UpgradeCreateDialogComponent,
    UpgradeEditDialogComponent,
  ],
  providers: [
    InstanceService,
    UpgradeService,
    FimService,
  ],
  entryComponents: [
    ExpiredPendingDialogComponent,
  ],
})
export class AdminInstancesModule { }
