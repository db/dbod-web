
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeModule } from '../../@theme/theme.module';
import { InstanceService } from '../../services/instance';
import { InstanceUpgradeDialogComponent } from './instance-upgrade-dialog/instance-upgrade-dialog.component';
// import { InlineEditorModule } from '@qontu/ngx-inline-editor';
// import { UiSwitchModule } from 'ngx-toggle-switch';
import { NgJsonEditorModule } from 'ang-jsoneditor';
import { InstanceComponent } from './instance.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { InstanceFileEditorComponent } from './instance-file-editor/instance-file-editor.component';
import { MetadataEditorComponent } from './metadata-editor/metadata-editor.component';
import { InstanceSnapshotsComponent } from './instance-snapshots/instance-snapshots.component';
import { InstanceJobsComponent } from './instance-jobs/instance-jobs.component';
import { RundeckService } from '../../services/rundeck/rundeck.service';
import { FileDownloaderService } from '../../services/file-downloader/file-downloader.service';
import { InstanceLogsComponent } from './instance-logs/instance-logs.component';
import { InstanceLogsStatisticsComponent } from './instance-logs/instance-logs-statistics/instance-logs-statistics.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PrettyJsonModule } from 'angular2-prettyjson';
import { ChartsModule } from 'ng2-charts';
import { MatSliderModule } from '@angular/material/slider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTabsModule } from '@angular/material/tabs';
import { InstanceDialogComponent } from './instance-dialog/instance-dialog.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatBadgeModule } from '@angular/material/badge';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { CalendarModule } from 'angular-calendar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InstanceRecoverDialogComponent } from './instance-snapshots/instance-recover-dialog/instance-recover-dialog.component';
import { InstanceRestoreDialogComponent } from './instance-snapshots/instance-restore-dialog/instance-restore-dialog.component';
import { InstanceBackupDialogComponent } from './instance-snapshots/instance-backup-dialog/instance-backup-dialog.component';
import { InstanceStartStopDialogComponent } from './instance-start-stop-dialog/instance-start-stop-dialog.component';
import { InstanceLoadFileDialogComponent } from './instance-file-editor/instance-load-file-dialog/instance-load-file-dialog.component';
import { InstanceUploadFileDialogComponent } from './instance-file-editor/instance-upload-file-dialog/instance-upload-file-dialog.component';
import { InstanceExpiryDateDialogComponent } from './instance-expiry-date-dialog/instance-expiry-date-dialog.component';
import {UpgradeService} from '../../services/upgrade';
import {MatSnackBarModule} from '@angular/material/snack-bar';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ThemeModule,
    // InlineEditorModule,
    NgJsonEditorModule,
    // UiSwitchModule,
    Ng2SmartTableModule,
    NgbModule,
    PrettyJsonModule,
    ChartsModule,
    MatSliderModule,
    MatExpansionModule,
    MatSelectModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTabsModule,
    MatFormFieldModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatBadgeModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    MatTooltipModule,
    MatButtonToggleModule,
    MatIconModule,
    MatMenuModule,
    CalendarModule,
    MatSnackBarModule,
  ],
  declarations: [
    InstanceComponent,
    InstanceDialogComponent,
    InstanceFileEditorComponent,
    MetadataEditorComponent,
    InstanceSnapshotsComponent,
    InstanceJobsComponent,
    InstanceLogsComponent,
    InstanceLogsStatisticsComponent,
    InstanceRecoverDialogComponent,
    InstanceRestoreDialogComponent,
    InstanceBackupDialogComponent,
    InstanceStartStopDialogComponent,
    InstanceLoadFileDialogComponent,
    InstanceUploadFileDialogComponent,
    InstanceExpiryDateDialogComponent,
    InstanceUpgradeDialogComponent,
  ],
  entryComponents: [
    InstanceDialogComponent,
    InstanceRecoverDialogComponent,
    InstanceRestoreDialogComponent,
    InstanceBackupDialogComponent,
    InstanceStartStopDialogComponent,
    InstanceLoadFileDialogComponent,
    InstanceUploadFileDialogComponent,
    InstanceExpiryDateDialogComponent,
    InstanceUpgradeDialogComponent,
  ],
  providers: [
    InstanceService,
    RundeckService,
    FileDownloaderService,
    UpgradeService,
  ],
})

export class InstanceModule { }
