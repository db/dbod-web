import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { InstanceService } from '../../../services/instance/instance.service';

@Component({
  selector: 'cern-ngx-instance-expiry-date-dialog',
  templateUrl: './instance-expiry-date-dialog.component.html',
})
export class InstanceExpiryDateDialogComponent {
  ExpiryDate = {
    PastDate: 0,
    HigherDate: 1,
    InvalidChange: 2,
  };

  expiry;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private instanceService: InstanceService) {
    this.expiry = data.expiryDate;
  }


}
