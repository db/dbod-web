import { Component, OnInit, Input, Inject, OnChanges, OnDestroy } from '@angular/core';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SocketJobs } from '../sockets.module';
import { AuthenticationService } from '../../../services/authentication/authentication.service';

@Component({
  selector: 'cern-ngx-instance-jobs',
  templateUrl: './instance-jobs.component.html',
  styleUrls: ['./instance-jobs.component.scss'],
  providers: [SocketJobs],
})
export class InstanceJobsComponent implements OnInit, OnChanges, OnDestroy {

  @Input() data: any;

  source = new Array();
  numberOfItems: number;
  pageLength: number;
  id: number;
  page: number;
  opened: boolean;
  loading: boolean = true;

  filters: string = '';

  constructor(private authService: AuthenticationService, @Inject(SocketJobs) private socket) {

  }

  ngOnInit() {
    this.socket.connect();

    this.opened = false;
    this.page = 1;
    this.pageLength = 10;

    this.socket.on('countjobs', (data) => {
      this.numberOfItems = JSON.parse(data);
    });

    this.socket.on('jobs', (data) => {
        this.source = JSON.parse(data);
        this.source.forEach(element => {
          delete element['id'];
          delete element['instance_id'];
          delete element['rundeck_id'];
        },
        );
      this.loading = false;
    });
  }
  refresh_jobs() {

    this.loading = true;
    this.authService.loadUser().then(() => {
      this.socket.emit('getter', {
        jwt: this.authService.user.jwt, id: this.id,
        size: this.pageLength, from: (this.page - 1) * this.pageLength, filters: this.filters,
      });
    });
  }
  pageChanged(page) {
    this.opened = false;
    if (!isNaN(page)) {
      this.refresh_jobs();
    }
  }

  changeItemsPerPage(e) {
    this.opened = false;
    this.pageLength = e.value;
    this.refresh_jobs();
  }

  panelOpened() {
    this.opened = true;
  }

  panelClosed() {
    this.opened = false;
  }

  ngOnChanges() {
    this.page = 1;
    this.pageLength = 10;
    if (this.data.hasOwnProperty('id')) {
      this.id = this.data.id;
      this.refresh_jobs();
    }
  }

  changeFilters(value) {
    this.page = 1;
    if (value.length !== 0) {
      this.filters = value;
    } else {
      this.filters = '';
    }
    this.opened = false;
    this.refresh_jobs();
  }

  ngOnDestroy() {
    this.socket.disconnect();
  }
}
