import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RundeckService } from '../../../../services/rundeck/rundeck.service';

@Component({
  selector: 'cern-ngx-instance-upload-file-dialog',
  templateUrl: './instance-upload-file-dialog.component.html',
})
export class InstanceUploadFileDialogComponent {
  State = {
    Confirm: 0,
    Loading: 1,
    Success: 2,
    Error: 3,
  };

  state = this.State['Confirm'];
  resMessage: string;
  resStatus: number;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private rundeckService: RundeckService) { }

  sendRequest() {
    this.state = this.State['Loading'];
    this.rundeckService.post('job/upload-file/' + this.data.instanceName + '?async', {
      'options': {
        'path': this.data.filepath,
        'base64string': btoa(this.data.file),
        'requester': this.data.requester,
      },
    }).then(() => {
      this.resMessage = 'Submission in progress, please check the jobs tab to see the results';
      this.resStatus = 200;
      this.state = this.State['Success'];
    }, (err: any) => {
      this.resMessage = err.message;
      this.resStatus = err.status;
      this.state = this.State['Error'];
    });
  }
}
