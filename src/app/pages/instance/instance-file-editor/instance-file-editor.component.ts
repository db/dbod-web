import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { RundeckService } from '../../../services/rundeck/rundeck.service';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { InstanceLoadFileDialogComponent } from './instance-load-file-dialog/instance-load-file-dialog.component';
import { InstanceUploadFileDialogComponent } from './instance-upload-file-dialog/instance-upload-file-dialog.component';
import { FileDownloaderService } from '../../../services/file-downloader/file-downloader.service';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'cern-ngx-instance-file-editor',
  templateUrl: './instance-file-editor.component.html',
  styleUrls: ['./instance-file-editor.component.scss'],
})
export class InstanceFileEditorComponent implements OnInit, OnChanges {

  @Input() data: any;

  configFilesList = [];
  selectedConfigFile = {
    title: null,
    filepath: null,
    content: null,
  };
  displayedContent = null;
  oldFile = null;
  validatedContents = null;
  valid: Boolean;
  invalid: Boolean;
  missing: Boolean;

  constructor(private rundeckService: RundeckService, private fileDownloaderService: FileDownloaderService,
    public dialog: MatDialog, private http: HttpClient, private authService: AuthenticationService) { }
  loadData() {
    this.configFilesList = [];
    this.displayedContent = null;
    this.selectedConfigFile = {
      title: null,
      filepath: null,
      content: null,
    };
    this.rundeckService.post('job/list-config-files/' + this.data.name).then((data: any) => {
      data.log.substr(2).slice(0, -2).split('\', \'').forEach((element) => {
        this.configFilesList.push({
          title: element.split('/').slice(-1)[0],
          filepath: element,
        });
      });
    }, err => console.log(err));
  }
  ngOnInit() {
    this.loadData();
  }

  selectConfigFile(data: { title: any; filepath: any; }) {
    this.selectedConfigFile = {
      title: data.title,
      filepath: data.filepath,
      content: null,
    };
    this.rundeckService.post('job/serve-file/' + this.data.name, {
      'options': {
        'filepath': this.selectedConfigFile.filepath,
      },
    }).then(async (data2: any) => {
      // this.http.get(data.log).subscribe(res => this.selectedConfigFile.content = res);
      this.selectedConfigFile.content = atob(data2.log);
      // var url = data.log + this.selectedConfigFile.filepath;
      // this.oldFile = await this.fileDownloaderService.saveFile(url, this.data.name);
      // this.selectedConfigFile.content = await this.fileDownloaderService.getConfigFile(this.oldFile);
      this.displayedContent = this.selectedConfigFile.content;
      // this.validateFile(this.displayedContent);
      this.invalid = false; // reset
      this.missing = false; // reset
      this.valid = true; // reset to true.
    }, err => console.log(err));
  }

  downloadConfigFile() {
    if (this.selectedConfigFile.content != null) {
      const blob = new Blob([this.selectedConfigFile.content]);
      FileSaver.saveAs(blob, this.data.name + '-' + this.selectedConfigFile.title);
    }
  }

  uploadConfigFile() {
    if (this.configFilesList.length !== 0) {
      const filesNamesList = [];
      this.configFilesList.forEach((element) => {
        filesNamesList.push(element.title);
      });
      const dialogRef = this.dialog.open(InstanceLoadFileDialogComponent, {
        data: {
          instanceName: this.data.name,
          filesNamesList: filesNamesList,
        },
      });

      dialogRef.afterClosed().subscribe(data => {
        if (data !== '') {
          this.selectedConfigFile = this.configFilesList.find((element) => {
            return element.title === data.title;
          });
          this.displayedContent = data.textContent;
          this.selectedConfigFile.content = this.displayedContent;
          this.validateFile(this.displayedContent);
        }
      });
    }
  }

  async validateFile(content: any) {
    if (content !== null) {
      this.validatedContents = await this.fileDownloaderService.getValidation(content,
        this.data.name, this.data.version, this.selectedConfigFile.title);
      this.invalid = !(this.validatedContents.invalid.length === 0);
      this.missing = !(this.validatedContents.missing.length === 0);
      this.valid = !(this.missing || this.invalid);
    }
  }

  submitChanges() {
    if (this.selectedConfigFile.title != null && this.displayedContent != null) {
      const file = this.selectedConfigFile;
      file.content = this.displayedContent;
      const dialogRef = this.dialog.open(InstanceUploadFileDialogComponent, {
        data: {
          title: file.title,
          instanceName: this.data.name,
          file: file.content,
          filepath: file.filepath,
          requester: this.authService.user.fullname,
        },
      });
    }
  }

  invalidate() {
    this.valid = false;
  }

  ngOnChanges() {
    // this.loadData();
  }
}
