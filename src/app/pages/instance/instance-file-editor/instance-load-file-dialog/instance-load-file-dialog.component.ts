import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RundeckService } from '../../../../services/rundeck/rundeck.service';

@Component({
  selector: 'cern-ngx-instance-load-file-dialog',
  templateUrl: './instance-load-file-dialog.component.html',
})
export class InstanceLoadFileDialogComponent {
  State = {
    Deposit: 0,
    Loading: 1,
    Success: 2,
    Error: 3,
  };

  state = this.State['Deposit'];
  resMessage: string;

  fileToUpload: File = null;
  textContent = '';

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private rundeckService: RundeckService) { }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  sendRequest() {
    if (this.fileToUpload != null) {
      this.state = this.State['Loading'];
      if (!this.data.filesNamesList.includes(this.fileToUpload.name)) {
        this.resMessage = 'Unknown configuration file. File should already exist in the files list, please check name';
        this.state = this.State['Error'];
      } else {
        const reader = new FileReader();
        reader.onload = (event) => {

          const resu = reader.result;
          if (typeof resu === 'string') { this.textContent = resu; }
          this.resMessage = 'File loaded with success';
          this.state = this.State['Success'];
        };
        reader.readAsText(this.fileToUpload);
      }
    }
  }
}
