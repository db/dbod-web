import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatDialogRef } from '@angular/material/dialog';
import { RundeckService } from '../../../../services/rundeck/rundeck.service';

@Component({
  selector: 'cern-ngx-instance-backup-dialog',
  templateUrl: './instance-backup-dialog.component.html',
})
export class InstanceBackupDialogComponent {
  State = {
    Confirm: 0,
    Loading: 1,
    Success: 2,
    Error: 3,
  };

  newTitle: string;
  state = this.State['Confirm'];
  resMessage: string;
  resStatus: number;

  constructor(public dialogRef: MatDialogRef<InstanceBackupDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private rundeckService: RundeckService) {
    dialogRef.beforeClosed().subscribe(() => {
      // console.log('in before close. this.state=' + this.state);
      dialogRef.close(this.state);
    });
  }

  sendRequest() {
    this.state = this.State['Loading'];
    this.rundeckService.post('job/backup/' + this.data.instanceName + '?async', {
      'options': {
        'instance': this.data.instanceName,
        requester: this.data.requester,
      },
    }).then((res: any) => {
      this.resMessage = 'The backup has started successfully. It can take a few minutes to complete.';
      this.resStatus = res.status;
      this.state = this.State['Success'];
    }, (err: any) => {
      this.resMessage = err.message;
      this.resStatus = err.status;
      this.state = this.State['Error'];
    });
  }
}
