import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { RundeckService } from '../../../../services/rundeck/rundeck.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'cern-ngx-instance-restore-dialog',
  templateUrl: './instance-restore-dialog.component.html',
  providers: [DatePipe],
})
export class InstanceRestoreDialogComponent {
  State = {
    Choose: 0,
    Confirm_restore: 1,
    Loading: 2,
    Success: 3,
    Error: 4,
  };

  newTitle: string;
  state = this.State['Choose'];
  resMessage: string;
  resStatus: number;
  restoreDate: Date = new Date();
  restoreHours: number;
  restoreMinutes: number;
  restoreTime: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private rundeckService: RundeckService, private datepipe: DatePipe) {
    this.restoreDate.setTime(Date.now());
    this.restoreHours = this.restoreDate.getHours();
    this.restoreMinutes = this.restoreDate.getMinutes();
    this.restoreTime = this.restoreHours + ':' + this.restoreMinutes;
  }
  formatDate(event: MatDatepickerInputEvent<Date>) {
    this.restoreDate = event.value;
    this.setRestoreTimeAndDate();
  }
  setRestoreTimeAndDate() {
    const splitted = this.restoreTime.split(':');
    this.restoreDate.setTime(this.restoreDate.getTime()
      + (parseInt(splitted[0], 10) - this.restoreDate.getHours()) * 3600 * 1000
      + (parseInt(splitted[1], 10) - this.restoreDate.getMinutes()) * 60 * 1000);
  }

  sendRestore() {
    const formattedDate = this.datepipe.transform(this.restoreDate, 'yyyy-MM-dd HH:mm:ss');
    console.log('formattedDate: ' + formattedDate);

    const req = {
      'options': {
        instance: this.data.instanceName,
        PITR: '--PITR',
        date: formattedDate,
        requester: this.data.requester,
      },
    };
    this.state = this.State['Loading'];
    this.rundeckService.post('job/recover/' + this.data.instanceName + '?async', req).then((res: any) => {
      this.resMessage = 'The restore has started successfully. It can take a few minutes to several hours to complete, depending on the size of the instance.';
      this.resStatus = res.status;
      this.state = this.State['Success'];
    }, (err: any) => {
      this.resMessage = err.message;
      this.resStatus = err.status;
      this.state = this.State['Error'];
    });
  }
}
