import { Component, ViewEncapsulation, Input, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';
import { RundeckService } from '../../../services/rundeck/rundeck.service';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import { CalendarEvent } from 'angular-calendar';
import {
  isSameDay,
  isSameMonth,
} from 'date-fns';
import { MatDialog } from '@angular/material/dialog';
import { InstanceRecoverDialogComponent } from './instance-recover-dialog/instance-recover-dialog.component';
import { InstanceRestoreDialogComponent } from './instance-restore-dialog/instance-restore-dialog.component';
import { InstanceBackupDialogComponent } from './instance-backup-dialog/instance-backup-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'cern-ngx-instance-snapshots',
  templateUrl: './instance-snapshots.component.html',
  styleUrls: ['./instance-snapshots.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class InstanceSnapshotsComponent implements OnInit, OnChanges {

  @Input() data: any;
  @Output() instanceScheduledBackupSettingsChange: EventEmitter<any> = new EventEmitter();

  view: string = 'month';
  viewDate: Date = new Date();
  activeDayIsOpen: boolean = false;
  loading: boolean = true;
  need_refresh_backups: number = 0;
  events = [];
  refreshing: boolean = false;

  constructor(private rundeckService: RundeckService, private authenticationService: AuthenticationService,
    public dialog: MatDialog, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.refreshBackups();
  }

  refreshBackups() {
    this.rundeckService.post('job/get-snapshots/' + this.data.name).then((data: any) => {
      if (this.data) {
        this.loading = false;
      }
      const calendarData = [];
      data.log.split(':').forEach((element) => {
        const day = element.split('_')[1].substring(0, 2);
        const month = element.split('_')[1].substring(2, 4);
        const year = element.split('_')[1].substring(4, 8);
        const hours = element.split('_')[2].substring(0, 2);
        const minutes = element.split('_')[2].substring(2, 4);
        const seconds = element.split('_')[2].substring(4, 6);
        const milliseconds = element.split('_')[3];

        // Month ordinals start from 0, so we need to substract one
        const date = new Date(year, month - 1, day, hours, minutes, seconds, milliseconds);

        calendarData.push({
          title: date.toString(),
          start: date,
        });
      });
      this.events = calendarData;
    }, err => console.log(err));
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }

  handleSnapshot(value) {
    const dialogRef = this.dialog.open(InstanceRecoverDialogComponent, {
      data: {
        instanceName: this.data.name,
        title: value.title,
        date: value.start,
        requester: this.authenticationService.user.fullname,
      },
    });
  }

  backup() {
    const dialogRef = this.dialog.open(InstanceBackupDialogComponent, {
      data: {
        instanceName: this.data.name,
        requester: this.authenticationService.user.fullname,
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      const resStatus = result;
      if (resStatus === dialogRef.componentInstance.State['Success']) {
        this.refreshBackups();
      }
    });
  }
  restore() {
    const dialogRef = this.dialog.open(InstanceRestoreDialogComponent, {
      data: {
        instanceName: this.data.name,
        requester: this.authenticationService.user.fullname,
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      const resStatus = result;
      if (resStatus === dialogRef.componentInstance.State['Success']) {
        this.refreshBackups();
      }
    });
  }

  ngOnChanges() {
    this.snackBar.open('Backup data is being loaded.', '', { duration: 3000 });
    this.refreshBackups();
  }
}
