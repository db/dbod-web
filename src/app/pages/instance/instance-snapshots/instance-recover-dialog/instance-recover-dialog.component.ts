import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RundeckService } from '../../../../services/rundeck/rundeck.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'cern-ngx-instance-recover-dialog',
  templateUrl: './instance-recover-dialog.component.html',
  providers: [DatePipe],
})
export class InstanceRecoverDialogComponent {
  State = {
    Choose: 0,
    Confirm_recover: 1,
    Loading: 2,
    Success: 3,
    Error: 4,
  };

  newTitle: string;
  state = this.State['Confirm_recover'];
  resMessage: string;
  resStatus: number;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private rundeckService: RundeckService, private datepipe: DatePipe) { }

  sendRecover() {
    const formattedDate = this.datepipe.transform(this.data.date, 'yyyy-MM-dd HH:mm:ss');
    const req = {
      'options': {
        instance: this.data.instanceName,
        date: formattedDate,
        requester: this.data.requester,
      },
    };

    this.state = this.State['Loading'];
    this.rundeckService.post('job/recover/' + this.data.instanceName + '?async', req).then((res: any) => { // TO EDIT
      this.resMessage = 'The restore has started successfully. It can take a few minutes to several hours to complete, depending on the size of the instance.';
      this.resStatus = res.status;
      this.state = this.State['Success'];
    }, (err: any) => {
      this.resMessage = err.message;
      this.resStatus = err.status;
      this.state = this.State['Error'];
    });
  }
}
