import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RundeckService } from '../../../services/rundeck/rundeck.service';
import { AuthenticationService } from '../../../services/authentication/authentication.service';

@Component({
  selector: 'cern-ngx-instance-start-stop-dialog',
  templateUrl: './instance-start-stop-dialog.component.html',
})
export class InstanceStartStopDialogComponent {
  State = {
    Confirm_start: 0,
    Confirm_stop: 1,
    Loading: 2,
    Success: 3,
    Error: 4,
  };

  state;
  resMessage: string;
  resStatus: number;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private rundeckService: RundeckService,
    private authService: AuthenticationService) {
    this.state = data.startStopFlag ? this.State['Confirm_start'] : this.State['Confirm_stop'];
  }

  sendRequest() {
    this.state = this.State['Loading'];
    const pathStartStop = this.data.startStopFlag ? 'start' : 'stop';
    const params = {
      options: {
        requester: this.authService.user.fullname,
      },
    };
    this.rundeckService.post('job/' + pathStartStop + '/' + this.data.instanceName + '?async', params).then((data: any) => {
      this.resStatus = data.status;
      this.resMessage = 'Job successfully submitted';
      this.state = this.State['Success'];
    }, (err) => {
      this.resStatus = err.status;
      this.resMessage = err.message;
      this.state = this.State['Error'];
    });
  }
}
