import { Component, OnInit, Input, AfterViewInit, ViewChild, ChangeDetectorRef, Inject, OnChanges, OnDestroy } from '@angular/core';
import { SocketLogs } from '../sockets.module';
import { RundeckService } from '../../../services/rundeck/rundeck.service';
import { FileDownloaderService } from '../../../services/file-downloader/file-downloader.service';
import { AuthenticationService } from '../../../services/authentication/authentication.service';
import * as FileSaver from 'file-saver';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatSlideToggle } from '@angular/material/slide-toggle';

@Component({
  selector: 'cern-ngx-instance-logs',
  templateUrl: './instance-logs.component.html',
  styleUrls: ['./instance-logs.component.scss'],
  providers: [SocketLogs],
})

export class InstanceLogsComponent implements OnInit, OnChanges, OnDestroy {

  @Input() data: any;
  @ViewChild(MatMenuTrigger)
  logFilesMenuTrigger: MatMenuTrigger;
  @ViewChild(MatSlideToggle)
  matSlideToggleRealTime: MatSlideToggle;
  source = new Array();
  numberOfItems: number;
  pageLength: number;
  dbName: string;
  logType: string;

  page: number;
  filters: string = '*';

  opened: boolean;
  public statisticsCollapsed = true;
  loading: boolean = true;

  logFilesList = [];

  constructor(private authService: AuthenticationService, @Inject(SocketLogs) private socket,
    private rundeckService: RundeckService, private fileDownloaderService: FileDownloaderService) {

  }
  loadData() {
    this.socket.connect();

    this.opened = false;
    this.page = 1;
    this.pageLength = 10;

    this.socket.on('countlogs', (data) => {
      this.numberOfItems = JSON.parse(data).count;
      // show the last 10000 items max, we need to change the way we retrieve from ES if we want more items
      this.numberOfItems = Math.min(this.numberOfItems, 10000);
    });

    this.socket.on('logs', (data) => {
      if (!this.opened) {
        this.source = JSON.parse(data);
        // console.log('receive');
      }
      this.loading = false;
    });
    this.page = 1;
    this.pageLength = 10;
    if (this.data.hasOwnProperty('type')) {
      this.dbName = this.data.name;
      switch (this.data.type) {
        case 'MYSQL': this.logType = 'mylog'; break;
        case 'InfluxDB': this.logType = 'influxlog'; break;
        case 'PG': this.logType = 'pglog'; break;
      }
      this.authService.loadUser().then(() => {
        this.socket.emit('getter', {
          jwt: this.authService.user.jwt, name: this.dbName, logType: this.logType, size: this.pageLength,
          from: (this.page - 1) * this.pageLength, filters: this.filters,
        });
      });
    }
  }

  ngOnInit() {
    this.loadData();
  }

  pageChanged(page) {
    this.loading = true;
    this.opened = false;
    if (!isNaN(page)) {
      this.authService.loadUser().then(() => {
        this.socket.emit('getter', {
          jwt: this.authService.user.jwt, name: this.dbName, logType: this.logType,
          size: this.pageLength, from: (this.page - 1) * this.pageLength, filters: this.filters,
        });
      });
    }
  }

  changeItemsPerPage(e) {
    this.loading = true;
    this.opened = false;
    this.pageLength = e.value;
    this.authService.loadUser().then(() => {
      this.socket.emit('getter', {
        jwt: this.authService.user.jwt, name: this.dbName, logType: this.logType,
        size: this.pageLength, from: (this.page - 1) * this.pageLength, filters: this.filters,
      });
    });
  }

  panelOpened() {
    this.opened = true;
  }

  panelClosed() {
    this.opened = false;
  }

  ngOnChanges() {
    this.loadData();
    // Close the MatMenu if it is open
    if (this.logFilesMenuTrigger !== undefined) {
      this.logFilesMenuTrigger.closeMenu();
    }
    // Uncheck the real time if it is enabled
    if (this.matSlideToggleRealTime !== undefined) {
      if (this.matSlideToggleRealTime.checked) {
        this.matSlideToggleRealTime.toggle();
      }
    }
  }

  realTimeHandler(e) {
    if (e.checked) {
      this.socket.emit('realtime_on');
      this.opened = false;
      this.authService.loadUser().then(() => {
        this.socket.emit('getter', {
          jwt: this.authService.user.jwt, name: this.dbName, logType: this.logType, size: this.pageLength,
          from: (this.page - 1) * this.pageLength, filters: this.filters,
        });
      });
    } else {
      this.socket.emit('realtime_off');
    }
  }

  changeFilters(value) {
    this.loading = true;
    this.page = 1;
    if (value.length !== 0) {
      this.filters = value;
    } else {
      this.filters = '*';
    }
    this.opened = false;
    this.authService.loadUser().then(() => {
      this.socket.emit('getter', {
        jwt: this.authService.user.jwt, name: this.dbName, logType: this.logType, size: this.pageLength,
        from: (this.page - 1) * this.pageLength, filters: this.filters,
      });
    });
  }

  listLogFiles() {
    this.logFilesList = [];
    if (this.logFilesList.length === 0) {
      this.rundeckService.post('job/list-log-files/' + this.data.name).then((data: any) => {
        data.log.substr(2).slice(0, -2).split('\', \'').forEach((element) => {
          this.logFilesList.push({
            title: element.split('/').slice(-1)[0],
            filepath: element,
          });
        });
      }, err => console.log(err));
    }
  }

  downloadLogFile(logFileData) {
    this.loading = true;
    this.rundeckService.post('job/serve-file/' + this.data.name, {
      'options': {
        'filepath': logFileData.filepath,
      },
    }).then(async (data2: any) => {
      const content = atob(data2.log);
      const blob = new Blob([content]);
      FileSaver.saveAs(blob, this.data.name + '-' + logFileData.title);
      this.loading = false;
    }, err => console.log(err));
  }

  ngOnDestroy() {
    this.socket.disconnect();
  }
}
