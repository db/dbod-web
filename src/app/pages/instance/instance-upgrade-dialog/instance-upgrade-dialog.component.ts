import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RundeckService } from '../../../services/rundeck/rundeck.service';

@Component({
  selector: 'cern-ngx-instance-upgrade-dialog',
  templateUrl: './instance-upgrade-dialog.component.html',
})
export class InstanceUpgradeDialogComponent {
  State = {
    Confirm_upgrade: 0,
    Loading: 1,
    Success: 2,
    Error: 3,
  };
  instanceData: any;
  state;
  resMessage: string;
  resStatus: number;
  upgradeTo: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private rundeckService: RundeckService) {
    this.state = this.State['Confirm_upgrade'];
    this.instanceData = data.instanceData;
    this.upgradeTo = data.upgradeTo;
  }

  sendRequest() {
    this.state = this.State['Loading'];
    let upgradePath = '';
    switch (this.instanceData.type) {
      case 'PG':
        upgradePath = 'upgrade-postgresql';
        break;
      case 'InfluxDB':
        upgradePath = 'upgrade-influxdb';
        break;
      case 'MYSQL':
        upgradePath = 'upgrade-mysql';
        break;
      default:
        upgradePath = '';
    }

    const params = {
      options : {
        version_from: this.instanceData.version,
        version_to: this.upgradeTo,
      },
    };

    this.rundeckService.post('job/' + upgradePath + '/' + this.instanceData.name + '?async', params)
      .then( (data: any) => {
        this.resStatus = data.status;
        this.resMessage = 'Job successfully submitted';
        this.state = this.State['Success'];
      }, (err) => {
        this.resStatus = err.status;
        this.resMessage = err.message;
        this.state = this.State['Error'];
      });
  }
}
