import { Component, OnInit, Input, ViewEncapsulation, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { InstanceDialogComponent } from './instance-dialog/instance-dialog.component';
import { InstanceStartStopDialogComponent } from './instance-start-stop-dialog/instance-start-stop-dialog.component';
import { InstanceExpiryDateDialogComponent } from './instance-expiry-date-dialog/instance-expiry-date-dialog.component';
import { SocketInstance } from './sockets.module';
import { AuthenticationService } from '../../services/authentication/authentication.service';
import { InstanceUpgradeDialogComponent } from './instance-upgrade-dialog/instance-upgrade-dialog.component';
import { InstanceService } from '../../services/instance';
import { UpgradeService } from '../../services/upgrade';

@Component({
  selector: 'cern-ngx-instance',
  styleUrls: ['./instance.scss'],
  templateUrl: './instance.html',
  encapsulation: ViewEncapsulation.None,
  providers: [SocketInstance ],
})

export class InstanceComponent implements OnInit {
  dbName: String;
  data: any = {};
  editedData: any = {};
  linkToMonitoring: String;
  buttonDisabled: boolean = false;
  restrictUpgrade: boolean = true;
  availableUpgrades = [];
  upgradeTo: String;
  expiryButtonDisabled: boolean = false;

  constructor(public authService: AuthenticationService, private route: ActivatedRoute, private router: Router,
    @Inject(SocketInstance) private socket, public dialog: MatDialog,
    private _instanceService: InstanceService, private _upgradeService: UpgradeService) {
  }

  loadData() {
    this.authService.loadUser().then(() => {
      this.socket.emit('getter', { jwt: this.authService.user.jwt, name: this.dbName });
    });
  }

  ngOnInit() {
    this.socket.connect();

    this.socket.on('error', (data) => {
      this.data = JSON.parse(data);
    });
    this.socket.on('instance', (data) => {
      this.data = JSON.parse(data);
      this.editedData = JSON.parse(data); // this.editedData = this.data doesn't give the same behaviour :o
      let type;
      switch (this.data.type) {
        case 'PG':
          type = 'postgresql';
          break;
        case 'InfluxDB':
          type = 'influxdb';
          break;
        case 'MYSQL':
          type = 'mysql';
          break;
        default:
          type = 'unknown';
      }
      this.linkToMonitoring = 'https://dbod-mon.web.cern.ch/dashboard/db/' + type + '-monitoring?var-server=' + this.data.host + '.cern.ch&var-instance=' + this.data.name;
      const currentDate = new Date();
      const maxDate = new Date(currentDate);
      const exp = this.data.expiry_date;
      const expiryDate = new Date(exp);
      this.expiryButtonDisabled = this.data.expiry_date === null && this.data.category === 'PROD';

      maxDate.setFullYear(maxDate.getFullYear() + 1);
      expiryDate.setMonth(expiryDate.getMonth() + 6);

      if (expiryDate > maxDate) {
        this.buttonDisabled = true;
      }
      this._upgradeService.getUpgrades().then(res => {
        this.availableUpgrades = res['response'];
        this.checkUpgrades();
      });
    });

    this.route.params.subscribe(params => {
      this.dbName = params['id'];
      this.loadData();
    });
  }

  changeField(name, value) {
    const dialogRef = this.dialog.open(InstanceDialogComponent, {
      data: {
        id: this.data.id,
        fieldName: name,
        attribute: false,
        precContent: this.data[name],
        newContent: value,
      },
    });
    dialogRef.afterClosed().subscribe(() => {
      this.loadData();
    });
  }

  changeAttributeField(name, value) {
    const dialogRef = this.dialog.open(InstanceDialogComponent, {
      data: {
        id: this.data.id,
        fieldName: name,
        attribute: true,
        precContent: this.data.attributes[name],
        newContent: value,
      },
    });
    dialogRef.afterClosed().subscribe(() => {
      this.loadData();
    });
  }

  checkDate(name, value) {
    const inputDate = new Date(value);
    const currentDate = new Date();
    const maxDate = new Date(currentDate);
    maxDate.setFullYear(maxDate.getFullYear() + 1);

    if (inputDate <= maxDate && inputDate >= currentDate) {
      this.buttonDisabled = false;
      return this.changeExpiryDate(name, inputDate);
    } else
      if (inputDate < currentDate) {
        const dialogRef = this.dialog.open(InstanceExpiryDateDialogComponent, {
          data: {
            expiryDate: 0,
          },
        });
        dialogRef.afterClosed().subscribe(() => {
          this.loadData();
        });
      } else if (inputDate > maxDate) {
        const dialogRef = this.dialog.open(InstanceExpiryDateDialogComponent, {
          data: {
            expiryDate: 1,
          },
        });
        dialogRef.afterClosed().subscribe(() => {
          this.loadData();
        });
      } else if (value === '' && this.data.category === 'PROD') {
        this.buttonDisabled = false;
        const dialogRef = this.dialog.open(InstanceDialogComponent, {
          data: {
            id: this.data.id,
            fieldName: name,
            attribute: false,
            precContent: this.data.attributes[name],
            newContent: null,
          },
        });
        dialogRef.afterClosed().subscribe(() => {
          this.loadData();
        });
      } else {
        const dialogRef = this.dialog.open(InstanceExpiryDateDialogComponent, {
          data: {
            expiryDate: 2,
          },
        });
        dialogRef.afterClosed().subscribe(() => {
          this.loadData();
        });
      }

  }

  changeExpiryDate(name, value) {
    const dialogRef = this.dialog.open(InstanceDialogComponent, {
      data: {
        id: this.data.id,
        fieldName: name,
        attribute: false,
        precContent: this.data[name],
        newContent: value,
      },
    });
    dialogRef.afterClosed().subscribe(() => {
      this.loadData();
    });
  }

  extendDate(name, value) {

    const dialogRef = this.dialog.open(InstanceDialogComponent, {
      data: {
        id: this.data.id,
        fieldName: name,
        attribute: false,
        precContent: this.data[name],
        newContent: this.extend(this.data[name]),
      },
    });
    dialogRef.afterClosed().subscribe(() => {
      this.loadData();
    });
  }

  extend(date) {
    if (date != null) {
      const expiryDate = new Date(date);
      expiryDate.setMonth(expiryDate.getMonth() + 6);
      return expiryDate;
    } else {
      const currentDate = new Date();
      currentDate.setMonth(currentDate.getMonth() + 6);
      return currentDate;
    }
  }
  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  startStopInstance(value) {
    const dialogRef = this.dialog.open(InstanceStartStopDialogComponent, {
      data: {
        instanceName: this.data.name,
        startStopFlag: value,
      },
    });
    dialogRef.afterClosed().subscribe(async () => {
      this.sleep(3000).then(() => { this.loadData(); });
    });
  }
  upgradeInstance() {
    const dialogRef = this.dialog.open(InstanceUpgradeDialogComponent, {
      data: {
        instanceData: this.data,
        upgradeTo: this.upgradeTo,
      },
    });
    dialogRef.afterClosed().subscribe(() => {
      this.sleep(3000).then(() => { this.loadData(); });
    });
  }
  checkUpgrades() {
    const applicableUpgrades = this.availableUpgrades.filter(upgrade => {
      if (this.data.version.toString() === upgrade['version_from']
        &&
        upgrade['category'] === this.data.category) {
        switch (this.data.type) {
          case 'PG':
            if (upgrade['db_type'] === 'PG') {
              return true;
            }
            break;
          case 'InfluxDB':
            if (upgrade['db_type'] === 'InfluxDB') {
              return true;
            }
            break;
          case 'MYSQL':
            if (upgrade['db_type'] === 'MYSQL') {
              return true;
            }
            break;
          default:
            return false;
        }
      }
    });

    if (applicableUpgrades.length > 0) {
      for (let i = 0; i < applicableUpgrades.length; i++) {
        this.upgradeTo = applicableUpgrades[i]['version_to'];
      }
      // Check if there exist a restrictive attribute

      if ('ready_for_upgrade' in this.data.attributes) {
        this.data.attributes['ready_for_upgrade'] === 'true' ?
          this.restrictUpgrade = false : this.restrictUpgrade = true;
      } else {
        this.restrictUpgrade = false;
      }
    }

  }
}
